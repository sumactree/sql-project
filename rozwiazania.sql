-- 1. Jaki jest laczny miesieczny koszt utrzymania wszystkich mieszkancow zoo?

SELECT sum(koszt)
FROM zwierze;

-- 2.Dla kazdego gatunku wypisz srednia laczna kwote wplat na przedstawiciela; posortuj malejaco wg. tej sredniej.

select zwierze.gatunek, avg(datek.ile)
from zwierze, datek
where zwierze. imie = datek.komu
group by zwierze. gatunek;

-- 3.Wypisz sponsorow, ktorzy miesiecznie wplacaja najwiecej.

select TOP 1 sum(datek.ile) as najwiecej, datek.kto
from datek
group by datek.kto
order by najwiecej desc;

-- 4.Wypisz wszystkie kontrakty, ktore mozna by rozwiazac przy zachowaniu pokrycia kosztow danego zwierzaka.

select sum(datek.ile), zwierze.imie
from datek
join zwierze
on datek.komu = zwierze.imie
group by zwierze.koszt, zwierze.imie
having sum(datek.ile)>zwierze.koszt;


-- 5.Dla kazdego sponsora, dla ktorego istnieje zwierze jego ulubionego gatunku nie otrzymujace zadnego datku, wypisz liczbe takich zwierzat.

select  sponsor.id, count(sponsor.id)
from sponsor
join datek
on datek.kto = sponsor.id
right join zwierze
on zwierze.imie = datek.komu
where datek.komu is null
group by sponsor.id
having count(sponsor.id) > 0;



